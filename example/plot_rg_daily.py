"""
Rg daily on flat ground
=======================

Plot rg_daily on specific locations to compare to fig1.10.1
"""
from math import radians

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from duffie2013.extraterrestrial import rg_daily

dates = ("2020-12-10",
         "2020-01-17",
         "2020-11-14",
         "2020-02-16",
         "2020-10-15",
         "2020-03-16",
         "2020-09-15",
         "2020-04-15",
         "2020-08-16",
         "2020-05-15",
         "2020-07-17",
         "2020-06-11")

records = []
for date in (pd.Timestamp(descr) for descr in dates):
    for latitude in np.linspace(0, 60, 100):
        records.append(dict(
            date=date,
            latitude=latitude,
            rg=rg_daily(date, radians(latitude))
        ))

df = pd.DataFrame(records)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(12, 6), squeeze=False)
ax = axes[0, 0]

for date, sdf in df.groupby('date'):
    ax.plot(sdf['latitude'], sdf['rg'], label=f"{date.date().isoformat()}")

ax.legend(loc='lower left')
ax.set_xlabel("Latitude [deg]")
ax.set_ylabel("daily rg [MJ.m-2]")

fig.tight_layout()
plt.show()
